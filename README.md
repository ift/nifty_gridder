NOTICE
======

Active development of `nifty_gridder` has stopped. The package will receive bug fixes
if necessary, but otherwise the code has been integrated into the `ducc0`
package (https://gitlab.mpcdf.mpg.de/mtr/ducc), and further development is
taking place there.

Please prefer `ducc0` over `nifty_gridder` if you are starting a new project!

Nifty gridder
=============

Library for high-accuracy gridding/degridding of radio interferometry datasets

(Highly experimental pre-release version!)

Programming aspects
- written in C++11, fully portable
- shared-memory parallelization via OpenMP and C++ threads.
- Python interface available
- kernel computation is performed on the fly, avoiding inaccuracies
  due to table lookup and reducing overall memory bandwidth

Numerical aspects
- uses the analytical gridding kernel presented in
  https://arxiv.org/abs/1808.06736
- uses the "improved W-stacking method" described in
  https://www.repository.cam.ac.uk/handle/1810/292298 (p. 139ff)
- in combination these two aspects allow extremely accurate gridding/degridding
  operations (L2 error compared to explicit DFTs can go below 1e-12) with
  reasonable resource consumption

Installation and prerequisites
- Clone the repository
- execute `pip3 install --user .`. This requires the g++ compiler.
  `numpy` and `pybind11` should be installed automatically if necessary.
  For the unit tests, `pytest` is required.
  The file `demo_wstack_realdata.py` requires `casacore` and measurement set
  data.
